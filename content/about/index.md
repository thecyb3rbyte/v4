---
title: 'About Me'
avatar: './me.jpg'
skills:
  - JavaScript (ES6+)
  - HTML & (S)CSS
  - React
  - Vue
  - Node.js
  - Express
  - NativeScript
---

Hello! I'm Sam Pandey, a aspirant of software engineer based in Boston, MA who enjoys building things that live on the internet. I develop exceptional websites and web apps that provide intuitive, pixel-perfect user interfaces with efficient and modern backends.

 I work on a wide variety of interesting and meaningful projects on a daily basis.

Here are a few technologies I've been working with recently:
